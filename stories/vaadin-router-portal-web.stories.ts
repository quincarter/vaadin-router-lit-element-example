import { html, TemplateResult } from 'lit';
import '../src/vaadin-router-portal-web.js';

export default {
  title: 'VaadinRouterPortalWeb',
  component: 'vaadin-router-portal-web',
  argTypes: {
    backgroundColor: { control: 'color' },
  },
};

interface Story<T> {
  (args: T): TemplateResult;
  args?: Partial<T>;
  argTypes?: Record<string, unknown>;
}

interface ArgTypes {
  title?: string;
  backgroundColor?: string;
}

const Template: Story<ArgTypes> = ({ title, backgroundColor = 'white' }: ArgTypes) => html`
  <vaadin-router-portal-web style="--vaadin-router-portal-web-background-color: ${backgroundColor}" .title=${title}></vaadin-router-portal-web>
`;

export const App = Template.bind({});
App.args = {
  title: 'My app',
};
