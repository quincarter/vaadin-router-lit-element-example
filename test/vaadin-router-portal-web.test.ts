import { html } from 'lit';
import { fixture, expect } from '@open-wc/testing';

import { VaadinRouterPortalWeb } from '../src/VaadinRouterPortalWeb.js';
import '../src/vaadin-router-portal-web.js';

describe('VaadinRouterPortalWeb', () => {
  let element: VaadinRouterPortalWeb;
  beforeEach(async () => {
    element = await fixture(html`<vaadin-router-portal-web></vaadin-router-portal-web>`);
  });

  it('renders a h1', () => {
    const h1 = element.shadowRoot!.querySelector('h1')!;
    expect(h1).to.exist;
    expect(h1.textContent).to.equal('My app');
  });

  it('passes the a11y audit', async () => {
    await expect(element).shadowDom.to.be.accessible();
  });
});
