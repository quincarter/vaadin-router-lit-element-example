/* eslint-disable import/extensions */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Router } from '@vaadin/router';
import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators.js';
import { routes } from './config/routes';

import './pages/home/my-home';

export class VaadinRouterPortalWeb extends LitElement {
  @property({ type: String }) title = 'My app';

  private _router: Router = new Router();

  static styles = css``;

  firstUpdated() {
    const outlet = this.shadowRoot?.getElementById('outlet');
    this._router = new Router(outlet);
    this._router.setRoutes(routes);
  }

  render() {
    return html`
      <div style="margin-bottom: 50px;">
        <a href="/">Home</a>
        <a href="/project/proj-1">Project</a>
        <a href="/organization/org-1">Organization</a>
      </div>
      <div id="outlet"></div>
    `;
  }
}
