/* eslint-disable @typescript-eslint/no-unused-vars */
import { Router } from '@vaadin/router';
import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators.js';
import arrayToTree from 'performant-array-to-tree';

export class VaadinRouterPortalWeb extends LitElement {
  @property({ type: String }) title = 'My app';

  private _router: Router;

  static styles = css``;

  constructor() {
    super();
    const outlet = this.shadowRoot?.getElementById('outlet');
    this._router = new Router(outlet);

    const array = [
      {
        type: 'route',
        alias: 'routeOrganization',
        elementName: 'ucp-organization',
        data: {
          path: '/organization/:organizationAlias',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationInformation',
        elementName: 'ucp-organization-information',
        data: {
          parent: 'routeOrganization',
          part: '/information',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationMembers',
        elementName: 'ucp-organization-members',
        data: {
          parent: 'routeOrganization',
          part: '/members',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationProjects',
        elementName: 'ucp-organization-projects',
        data: {
          parent: 'routeOrganization',
          part: '/projects',
        },
      },
    ];

    const tree = arrayToTree(array, {
      id: 'alias',
      parentId: 'data.parent',
      dataField: null,
    });

    console.log('ARRAY', array);
    console.log(tree);
  }

  firstUpdated() {
    const outlet = this.shadowRoot?.getElementById('outlet');
    this._router = new Router(outlet);
    this._router.setRoutes([
      {
        name: 'home',
        path: '/',
        component: 'my-home',
      },
      {
        name: 'project',
        path: '/project/:projectAlias',
        component: 'my-project',
        children: [
          {
            name: 'projectInformation',
            path: '/information',
            component: 'my-project-information',
          },
          {
            name: 'projectHostnames',
            path: '/hostnames',
            component: 'my-project-hostnames',
          },
        ],
      },
      {
        name: 'organization',
        path: '/organization/:organizationAlias',
        component: 'my-organization',
        children: [
          {
            name: 'organizationInformation',
            path: '/information',
            component: 'my-organization-information',
            children: [],
          },
          {
            name: 'organizationPayment',
            path: '/payment',
            component: 'my-organization-payment',
            children: [],
          },
          {
            name: 'organizationMembers',
            path: '/members',
            component: 'my-organization-members',
            children: [
              {
                name: 'organizationMember',
                path: '/:id',
                component: 'my-organization-member',
                children: [],
              },
            ],
          },
        ],
      },
    ]);
  }

  render() {
    return html`
      <div style="margin-bottom: 50px;">
        <a href="/">Home</a>
        <a href="/project/proj-1">Project</a>
        <a href="/organization/org-1">Organization</a>
      </div>
      <div id="outlet"></div>
    `;
  }
}
