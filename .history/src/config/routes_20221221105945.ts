import { Route } from '@vaadin/router';
import { ORG_ROUTES } from './orgs';
import { PROJECT_ROUTES } from './projects';

export const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: 'my-home',
  },
  { ...PROJECT_ROUTES, ...ORG_ROUTES },

  { path: '(.*)', component: 'my-home' }, // 404 not found routes
];
