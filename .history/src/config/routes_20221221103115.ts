import { Route } from '@vaadin/router';

export const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: 'my-home',
  },
  {
    name: 'project',
    path: '/project/:projectAlias',
    component: 'my-project',
    children: [
      {
        name: 'projectInformation',
        path: '/information',
        component: 'project-info',
      },
      {
        name: 'projectHostnames',
        path: '/hostnames',
        component: 'project-hostnames',
      },
    ],
  },
  {
    name: 'organization',
    path: '/organization/:organizationAlias',
    component: 'my-org',
    children: [
      {
        name: 'organizationInformation',
        path: '/information',
        component: 'my-organization-information',
        children: [],
      },
      {
        name: 'organizationPayment',
        path: '/payment',
        component: 'my-organization-payment',
        children: [],
      },
      {
        name: 'organizationMembers',
        path: '/members',
        component: 'my-organization-members',
        children: [
          {
            name: 'organizationMember',
            path: '/:id',
            component: 'my-organization-member',
            children: [],
          },
        ],
      },
    ],
  },
];
