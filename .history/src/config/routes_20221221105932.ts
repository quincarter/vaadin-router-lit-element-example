import { Route } from '@vaadin/router';

export const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: 'my-home',
  },
  {}
  { path: '(.*)', component: 'my-home' }, // 404 not found routes
];
