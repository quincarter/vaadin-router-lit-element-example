import { Route } from '@vaadin/router';
import { PROJECT_ROUTES } from './projects';

export const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: 'my-home',
  },
  {...PROJECT_ROUTES}
  { path: '(.*)', component: 'my-home' }, // 404 not found routes
];
