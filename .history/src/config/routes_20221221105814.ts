import { Route } from '@vaadin/router';

export const routes: Route[] = [
  {
    name: 'home',
    path: '/',
    component: 'my-home',
  },
  {
    name: 'project',
    path: '/(project[s]?)/:projectAlias',
    component: 'my-project',
    children: [
      {
        name: 'projectInformation',
        path: '/information',
        component: 'project-info',
      },
      {
        name: 'projectHostnames',
        path: '/hostnames',
        component: 'project-hostnames',
      },
    ],
  },
  
  { path: '(.*)', component: 'my-home' },
];
