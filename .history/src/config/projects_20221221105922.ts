import { Route } from '@vaadin/router';

export const PROJECT_ROUTES: Route = {
  name: 'project',
  path: '/(project[s]?)/:projectAlias',
  component: 'my-project',
  children: [
    {
      name: 'projectInformation',
      path: '/information',
      component: 'project-info',
    },
    {
      name: 'projectHostnames',
      path: '/hostnames',
      component: 'project-hostnames',
    },
  ],
};
