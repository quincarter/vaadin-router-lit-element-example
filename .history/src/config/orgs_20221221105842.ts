export const ORG_ROUTES = {
    name: 'organization',
    path: '/organization/:organizationAlias',
    component: 'my-org',
    children: [
      {
        name: 'organizationInformation',
        path: '/information',
        component: 'my-organization-information',
        children: [],
      },
      {
        name: 'organizationPayment',
        path: '/payment',
        component: 'my-organization-payment',
        children: [],
      },
      {
        name: 'organizationMembers',
        path: '/members',
        component: 'my-organization-members',
        children: [
          {
            name: 'organizationMember',
            path: '/:id',
            component: 'my-organization-member',
            children: [],
          },
        ],
      },
    ],
  }
