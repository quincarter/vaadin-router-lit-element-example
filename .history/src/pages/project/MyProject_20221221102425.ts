/* eslint-disable import/extensions */
import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators';

import './project-hostnames/project-hostnames';
import './project-info/project-info';

export class MyProject extends LitElement {
  render() {
    return html`<div>
      <h1>Project Hostnames</h1>
    </div>`;
  }
}
