import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators';

export class Project extends LitElement {
  render() {
    return html`div style="display: flex; gap: 30px;">
    <div style="display: flex; flex-direction: column; gap: 10px;">
      <a style="display: block;" href="/project/proj-1/information">Information</a>
      <a style="display: block;" href="/project/proj-2/hostnames">Hostnames</a>
    </div>
    <div>
      <slot></slot>
    </div>
  </div>`;
  }
}
