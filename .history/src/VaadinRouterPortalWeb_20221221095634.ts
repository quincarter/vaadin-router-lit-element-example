import { Router } from '@vaadin/router';
import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators.js';

const logo = new URL('../../assets/open-wc-logo.svg', import.meta.url).href;

export class VaadinRouterPortalWeb extends LitElement {
  @property({ type: String }) title = 'My app';

  private _router: Router;

  static styles = css`
  `;

  constructor() {
    super();
    const router = new Router();

    const array = [
      {
        type: 'route',
        alias: 'routeOrganization',
        elementName: 'ucp-organization',
        data: {
          path: '/organization/:organizationAlias',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationInformation',
        elementName: 'ucp-organization-information',
        data: {
          parent: 'routeOrganization',
          part: '/information',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationMembers',
        elementName: 'ucp-organization-members',
        data: {
          parent: 'routeOrganization',
          part: '/members',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationProjects',
        elementName: 'ucp-organization-projects',
        data: {
          parent: 'routeOrganization',
          part: '/projects',
        },
      },
    ];

    const tree = arrayToTree(array, {
      id: 'alias',
      parentId: 'data.parent',
      dataField: null,
    });

    console.log('ARRAY', array);
    console.log(tree);
  }

  render() {
    return html`
      <main>
        <div class="logo"><img alt="open-wc logo" src=${logo} /></div>
        <h1>${this.title}</h1>

        <p>Edit <code>src/VaadinRouterPortalWeb.ts</code> and save to reload.</p>
        <a
          class="app-link"
          href="https://open-wc.org/guides/developing-components/code-examples"
          target="_blank"
          rel="noopener noreferrer"
        >
          Code examples
        </a>
      </main>

      <p class="app-footer">
        🚽 Made with love by
        <a
          target="_blank"
          rel="noopener noreferrer"
          href="https://github.com/open-wc"
          >open-wc</a
        >.
      </p>
    `;
  }
}
