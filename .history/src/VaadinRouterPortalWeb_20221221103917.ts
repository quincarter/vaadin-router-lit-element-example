/* eslint-disable import/extensions */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Router } from '@vaadin/router';
import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators.js';
import { routes } from './config/routes';

import './pages/home/my-home';
import './pages/project/my-project';
import './pages/org/my-org';
import './pages/project/project-hostnames/project-hostnames';
import './pages/project/project-info/project-info';

export class VaadinRouterPortalWeb extends LitElement {
  @property({ type: String }) title = 'My app';

  private _router: Router = new Router();

  static styles = css``;

  constructor() {
    super();
    const router = new Router();

    const array = [
      {
        type: 'route',
        alias: 'routeOrganization',
        elementName: 'ucp-organization',
        data: {
          path: '/organization/:organizationAlias',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationInformation',
        elementName: 'ucp-organization-information',
        data: {
          parent: 'routeOrganization',
          part: '/information',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationMembers',
        elementName: 'ucp-organization-members',
        data: {
          parent: 'routeOrganization',
          part: '/members',
        },
      },
      {
        type: 'route',
        alias: 'routeOrganizationProjects',
        elementName: 'ucp-organization-projects',
        data: {
          parent: 'routeOrganization',
          part: '/projects',
        },
      },
    ];

    console.log('ARRAY', array);
    console.log(tree);
  }

  firstUpdated() {
    const outlet = this.shadowRoot?.getElementById('outlet');
    this._router = new Router(outlet);
    this._router.setRoutes(routes);
  }

  render() {
    return html`
      <div style="margin-bottom: 50px;">
        <a href="/">Home</a>
        <a href="/project/proj-1">Project</a>
        <a href="/organization/org-1">Organization</a>
      </div>
      <div id="outlet"></div>
    `;
  }
}
