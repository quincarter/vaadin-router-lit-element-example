import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators';

export class MyHome extends LitElement {
  render() {
    return html`<p>Home works!</p>`;
  }
}
