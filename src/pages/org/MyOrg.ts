import { LitElement, html, css } from 'lit';
import { property } from 'lit/decorators';

export class MyOrg extends LitElement {
  render() {
    return html`<p>Org works!</p>`;
  }
}
